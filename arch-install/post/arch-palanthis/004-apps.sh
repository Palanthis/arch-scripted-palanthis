#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

sudo pacman -S --needed --noconfirm gimp
sudo pacman -S --needed --noconfirm youtube-dl
sudo pacman -S --needed --noconfirm keepass
sudo pacman -S --needed --noconfirm dropbox
sudo pacman -S --needed --noconfirm audacity
sudo pacman -S --needed --noconfirm flameshot
sudo pacman -S --needed --noconfirm grsync
sudo pacman -S --needed --noconfirm handbrake
sudo pacman -S --needed --noconfirm hunspell-en
sudo pacman -S --needed --noconfirm inkscape
sudo pacman -S --needed --noconfirm jdk8-openjdk
sudo pacman -S --needed --noconfirm netbeans
sudo pacman -S --needed --noconfirm rhythmbox
sudo pacman -S --needed --noconfirm wget
sudo pacman -S --needed --noconfirm gitkraken


